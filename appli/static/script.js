let ongletCircuit = document.getElementById('onglet_circuit');
let ongletCode = document.getElementById('onglet_code');
let ongletCode2 = document.getElementById('onglet_code2');
let ongletHtml = document.getElementById('onglet_html');
let ongletCss = document.getElementById('onglet_css');
let ongletJs = document.getElementById('onglet_js');
let schema = document.getElementById('schemas');
let blockCode = document.getElementById('block_code');
let blockCode2 = document.getElementById('block_code2');
let blockHtml = document.getElementById('block_html');
let blockCss = document.getElementById('block_css');
let blockJs = document.getElementById('block_js');

function circuitTop() {
  if(ongletCircuit) {
    ongletCircuit.style.backgroundColor = 'LightGoldenRodYellow';
  }
  if(ongletCode) {
    ongletCode.style.backgroundColor = 'white';
  }
  if(ongletCode2) {
    ongletCode2.style.backgroundColor = 'white';
  }
  if(ongletHtml) {
    ongletHtml.style.backgroundColor = 'white';
  }
  if(ongletCss) {
    ongletCss.style.backgroundColor = 'white';
  }
  if(ongletJs) {
    ongletJs.style.backgroundColor = 'white';
  }
  if(schema) {
    schema.style.zIndex = 10000;
  }
  if(blockCode) {
    blockCode.style.zIndex = 0;
  }
  if(blockCode2) {
    blockCode2.style.zIndex = 0;
  }
  if(blockHtml) {
    blockHtml.style.zIndex = 0;
  }
  if(blockCss) {
    blockCss.style.zIndex = 0;
  }
  if(blockJs) {
    blockJs.style.zIndex = 0;
  }
}

function codeTop() {
  if(ongletCircuit) {
    ongletCircuit.style.backgroundColor = 'white';
  }
  if(ongletCode) {
    ongletCode.style.backgroundColor = '#ffe6cc';
  }
  if(ongletCode2) {
    ongletCode2.style.backgroundColor = 'white';
  }
  if(ongletHtml) {
    ongletHtml.style.backgroundColor = 'white';
  }
  if(ongletCss) {
    ongletCss.style.backgroundColor = 'white';
  }
  if(ongletJs) {
    ongletJs.style.backgroundColor = 'white';
  }
  if(schema) {
    schema.style.zIndex = 0;
  }
  if(blockCode) {
    blockCode.style.zIndex = 10000;
  }
  if(blockCode2) {
    blockCode2.style.zIndex = 0;
  }
  if(blockHtml) {
    blockHtml.style.zIndex = 0;
  }
  if(blockCss) {
    blockCss.style.zIndex = 0;
  }
  if(blockJs) {
    blockJs.style.zIndex = 0;
  }
}

function code2Top() {
  if(ongletCircuit) {
    ongletCircuit.style.backgroundColor = 'white';
  }
  if(ongletCode) {
    ongletCode.style.backgroundColor = 'white';
  }
  if(ongletCode2) {
    ongletCode2.style.backgroundColor = '#ffe6cc';
  }
  if(ongletHtml) {
    ongletHtml.style.backgroundColor = 'white';
  }
  if(ongletCss) {
    ongletCss.style.backgroundColor = 'white';
  }
  if(ongletJs) {
    ongletJs.style.backgroundColor = 'white';
  }
  if(schema) {
    schema.style.zIndex = 0;
  }
  if(blockCode) {
    blockCode.style.zIndex = 0;
  }
  if(blockCode2) {
    blockCode2.style.zIndex = 10000;
  }
  if(blockHtml) {
    blockHtml.style.zIndex = 0;
  }
  if(blockCss) {
    blockCss.style.zIndex = 0;
  }
  if(blockJs) {
    blockJs.style.zIndex = 0;
  }
}

function htmlTop() {
  if(ongletCircuit) {
    ongletCircuit.style.backgroundColor = 'white';
  }
  if(ongletCode) {
    ongletCode.style.backgroundColor = 'white';
  }
  if(ongletCode2) {
    ongletCode2.style.backgroundColor = 'white';
  }
  if(ongletHtml) {
    ongletHtml.style.backgroundColor = '#e6fff3';
  }
  if(ongletCss) {
    ongletCss.style.backgroundColor = 'white';
  }
  if(ongletJs) {
    ongletJs.style.backgroundColor = 'white';
  }
  if(schema) {
    schema.style.zIndex = 0;
  }
  if(blockCode) {
    blockCode.style.zIndex = 0;
  }
  if(blockCode2) {
    blockCode2.style.zIndex = 0;
  }
  if(blockHtml) {
    blockHtml.style.zIndex = 10000;
  }
  if(blockCss) {
    blockCss.style.zIndex = 0;
  }
  if(blockJs) {
    blockJs.style.zIndex = 0;
  }
}

function cssTop() {
  if(ongletCircuit) {
    ongletCircuit.style.backgroundColor = 'white';
  }
  if(ongletCode) {
    ongletCode.style.backgroundColor = 'white';
  }
  if(ongletCode2) {
    ongletCode2.style.backgroundColor = 'white';
  }
  if(ongletHtml) {
    ongletHtml.style.backgroundColor = 'white';
  }
  if(ongletCss) {
    ongletCss.style.backgroundColor = '#e6fff3';
  }
  if(ongletJs) {
    ongletJs.style.backgroundColor = 'white';
  }
  if(schema) {
    schema.style.zIndex = 0;
  }
  if(blockCode) {
    blockCode.style.zIndex = 0;
  }
  if(blockCode2) {
    blockCode2.style.zIndex = 0;
  }
  if(blockHtml) {
    blockHtml.style.zIndex = 0;
  }
  if(blockCss) {
    blockCss.style.zIndex = 10000;
  }
  if(blockJs) {
    blockJs.style.zIndex = 0;
  }
}

function jsTop() {
  if(ongletCircuit) {
    ongletCircuit.style.backgroundColor = 'white';
  }
  if(ongletCode) {
    ongletCode.style.backgroundColor = 'white';
  }
  if(ongletCode2) {
    ongletCode2.style.backgroundColor = 'white';
  }
  if(ongletHtml) {
    ongletHtml.style.backgroundColor = 'white';
  }
  if(ongletCss) {
    ongletCss.style.backgroundColor = 'white';
  }
  if(ongletJs) {
    ongletJs.style.backgroundColor = '#e6fff3';
  }
  if(schema) {
    schema.style.zIndex = 0;
  }
  if(blockCode) {
    blockCode.style.zIndex = 0;
  }
  if(blockCode2) {
    blockCode2.style.zIndex = 0;
  }
  if(blockHtml) {
    blockHtml.style.zIndex = 0;
  }
  if(blockCss) {
    blockCss.style.zIndex = 0;
  }
  if(blockJs) {
    blockJs.style.zIndex = 10000;
  }
}
